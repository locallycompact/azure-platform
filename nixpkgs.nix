import (builtins.fetchTarball {
  name = "nixos-unstable-2018-12-25";
  url = "https://github.com/nixos/nixpkgs/archive/aac952c48a2a827240f19ed190f668b2e407de55.tar.gz";
  sha256 = "0v0d66xh1ag0f056sk99v021fc2qia9mnn67ym88yw9w0jlnw1n0";
})
