{ pkgs ? import ./nixpkgs.nix {} }:

with pkgs;
with dockerTools;

let

  nixBaseImage = pullImage {
    imageName = "nixos/nix";
    finalImageTag = "latest"; 
    imageDigest = "sha256:1b97dc98b599779d80271d4203275627c0749d8bd5e9b9c868154249395cbc1b";
    sha256 = "134h20xgpqchcvgv0f13knsi2p7jfl4m9mk8hkvdgh8hnz0hj3hc";
  };

  passwd = ''
    root:x:0:0::/root:/run/current-system/sw/bin/bash
    ${builtins.concatStringsSep "\n" (builtins.genList (i: "nixbld${toString (i+1)}:x:${toString (i+30001)}:30000::/var/empty:/run/current-system/sw/bin/nologin") 32)}
  '';

  group = ''
    root:x:0:
    nogroup:x:65534:
    nixbld:x:30000:${builtins.concatStringsSep "," (builtins.genList (i: "nixbld${toString (i+1)}") 32)}
  '';

  azure-env = stdenv.mkDerivation {
    name = "zenhaskell-user-environment";
    phases = ["installPhase" "fixupPhase"];
    installPhase = ''
      mkdir -p $out/etc
      echo '${group}' > $out/etc/group
      echo '${passwd}' > $out/etc/passwd
    '';
  };

  stdConf = {
    Cmd = "bash";
    Env = [ "LANG=en_US.UTF-8"
            "LC_ALL=en_US.UTF-8"
            "USER=root"
            "ENV=/etc/profile"
            "FONTCONFIG_PATH=/etc/fonts"
            "PATH=/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin"
            "NIX_PATH=/nix/var/nix/profiles/per-user/root/channels"
            "GIT_SSL_CAINFO=${cacert}/etc/ssl/certs/ca-bundle.crt"
            "NIX_SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
            "LOCALE_ARCHIVE=${glibcLocales}/lib/locale/locale-archive"
          ];
  };
in
rec {
  stackBaseImage = buildImage {
    name = "locallycompact/azureplatform";
    tag = "latest";
    fromImage = nixBaseImage;

    contents = [ azure-env cacert nix bash cachix stack git dhall dhall-json wget glibcLocales packer powershell azure-cli terraform vault consul ];

    config = stdConf;
  };
} 
