# Azure Docker Infrastructure

Nix based docker gear designed to make working with Azure less painful, from
gitlab at least.

Contains powershell, the azure-cli and the hashicorp set, along with dhall, stack, cachix etc.

# Building

There is one image currently, but can extend to many. Any images you may build
with `nix-build azure-platform.nix`. They will be output to `result(-n)`,
import them in to docker with `docker load < result`.
